
{
    'name': 'Report Assignment',
    'version': '1.0',
    'author': 'Odoo Assignment',
    'summary': 'Customer, Product, Sales',
    'description': """
    Manage sales quotations and orders
==================================

This application allows you to manage your sales goals in an effective and efficient manner by keeping track of all sales orders and history.

It handles the full sales workflow:

It adds the customer Information




    """,
    'website': 'www.pragtech.co.in',
    'depends': ['base'],
    'demo': [
        
    ],   
    'data': [
                'views/sale_order_sequence.xml',
                'views/customer.xml',
                'views/product.xml',
                'views/sale_product.xml',
                'views/menu_item.xml',
                'report/report_sale_order_document.xml',
                
                
             
    ],
    'auto_install': False,
    'application': True,
    "installable": True,
    

}
