from openerp import fields, api, models
from openerp.exceptions import UserError
from datetime import date
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta





class customer_info(models.Model):
    
    _name = 'customer.info'
    
    name = fields.Char(string ="Customer Name", size=100, required=True, help="Enter customer name here")
    #company_id = fields.Many2one('customer.companye', 'Company')
   

    email = fields.Char(sting ="Email", size=100, required=True )
    phone = fields.Char('Contact number', size=16)
    country_id = fields.Many2one('res.country', 'Country')
    state_id = fields.Many2one('res.country.state', 'State')
    date_of_birth = fields.Date("Date of Birth")
    age = fields.Char('Age')
    street = fields.Char('Street')
    city = fields.Char('City')
    zip_code = fields.Char('Zip')
    
    @api.onchange('date_of_birth')
    def set_age(self):
        for rec in self:
            if rec.date_of_birth:
                dt = rec.date_of_birth
                d1 = datetime.strptime(dt, "%Y-%m-%d").date()
                d2 = date.today()
                rd = relativedelta(d2, d1)
                rec.age = str(rd.years) + ' years' 