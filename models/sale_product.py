from openerp import fields, api, models
from openerp.exceptions import UserError
from datetime import datetime, timedelta
from openerp.exceptions import UserError, ValidationError

class sale_oders(models.Model):
    

    _name = 'sale.orders'
    
    
    
    
#     sale_order_auto_id =fields.Char('Sale Order Auto Id',default=lambda obj:
#         obj.env['ir.sequence'].next_by_code('sale.orders'), readonly = 'True')
        
    
    @api.model            
    def create(self,vals):
        vals['sale_order_auto_id']=self.env['ir.sequence'].next_by_code('sale.orders') 
        return super(sale_oders,self).create(vals)           
    
            
    @api.multi
    def confirm_multiple_order(self):
        sale_order_id = self.env['sale.orders'].browse(
            self._context.get('active_ids'))
        for order in sale_order_id:
            if order.state == 'draft':
                order.write({'state': 'paid'})
                
    @api.multi
    def create_invoice(self, *args):
        for obj in self:
            print "obj------",obj
#             acc_id = obj.partner_id.property_account_receivable_id.id
            journal_obj = self.env['account.journal']
            journal_ids = journal_obj.search([('type', '=', 'sale')], limit=1)

            journal_id = None
            if journal_ids[0]:
                journal_id = journal_ids[0].id

            if obj:
                inv = {
                    'name': obj.sale_order_auto_id,
                    'origin': obj.sale_order_auto_id,
                    'type': 'out_invoice',
                    'reference': "Order Invoice",
                    'account_id': self.env['account.account'].search([('user_type_id', '=', self.env.ref('account.data_account_type_revenue').id)], limit=1).id,
                    'partner_id': obj.partner_id.id,
                    'currency_id': self.env['res.currency'].search([('name', '=', 'EUR')])[0].id,
                    'journal_id': journal_id,
                    'amount_tax': obj.tax_amount,
                    'amount_untaxed': obj.amount_subtotal,
                    'amount_total': obj.total,
                }
                print "inv---------",inv
                inv_id = self.env['account.invoice'].create(inv)

                for ol in obj.order_line:

                    a = self.env['ir.property'].get(
                            'property_account_income_categ_id', 'product.category').id
                    il = {
                        'name': ol.order_id,
                        'account_id': a,
                        'price_unit': ol.price_unit,
                        'quantity': ol.quantity,
                        'origin': obj.sale_order_auto_id,
                        'invoice_id': inv_id.id,
                        'price_subtotal': ol.subtotal,
                    }
                    print "il=========",il
                    self.env['account.invoice.line'].create(il)
        self.write({'state': 'paid'})
        return True    
        
    @api.multi
    def get_delivered(self):
        for self_obj in self:
            picking_type_ids = self.env["stock.picking.type"].search([('code', '=', 'outgoing'), ('name', '=', 'Delivery Orders')])
            print "picking_type_idssssssssssssssss",picking_type_ids[0]
            if picking_type_ids:
                picking_data={
                          'name': self.env['ir.sequence'].next_by_code('stock.picking.out'),
                          'origin':self_obj.sale_order_auto_id,
                          'state':'draft',
                          'picking_type_id':picking_type_ids[0].id,
                          'location_dest_id':self_obj.partner_id.id,
                          'state':'assigned',
                          }
                print "picking_dataddddd",picking_data
            picking_id=self.env['stock.picking'].create(picking_data)   
        self.write({'state': 'delivered'})                 
                
        
    @api.multi
    def get_cancel(self):
        for payment in self:
            payment.write({'state': 'cancel'})

              
    @api.one
    def _get_total_discount(self):
        total = 0
        for order in self.order_line:           
            self.total_discount = self.total_discount + order.discount   
            
    @api.onchange('order_line.subtotal') 
    @api.one 
    def _get_totat_tax(self): 
        val = 0.00
        for order in self.order_line:          
            val = self.tax_amount + order.product_tax
        self.tax_amount = (order.subtotal * val)/100  
        
    @api.onchange('order_line.subtotal') 
    @api.one       
    def _get_subtotal(self):
        for order in self.order_line:    
            self.amount_subtotal = self.amount_subtotal + order.subtotal
                
    @api.onchange('order_line.discount')  
    @api.one
    def _get_total(self):
        for dis in self.order_line:           
            self.total = self.tax_amount + dis.subtotal 
                   

    sale_order_auto_id = fields.Char(string="Sale Order Auto Id", readonly = 'True')
    partner_id = fields.Many2one('customer.info', string='Customer')  
    user_id = fields.Many2one('res.users', string='Salesperson', index=True, track_visibility='onchange', default=lambda self: self.env.user)
    order_line = fields.One2many('sale.orderlineee','order_id', string ='order lines')
    name = fields.Text(string= "Description")
    state = fields.Selection([('draft', 'Draft'), ('cancel', 'Cancel'), 
                                ('paid', 'Paid'), ('delivered', 'Deliver')],
                              string='Status', default="draft")
    amount_subtotal = fields.Float(compute= "_get_subtotal", string= 'SubTotal', readonly = 'True')
    tax_amount = fields.Float(compute= "_get_totat_tax", string= 'Tax', readonly = 'True')
    total_discount = fields.Float(compute='_get_total_discount', string= 'Discount', readonly = 'True')
    total = fields.Float(compute='_get_total', string= 'Total', readonly = 'True')


            



 
class sale_orderline(models.Model):
    _name = 'sale.orderlineee'

    @api.onchange('product_id')
    def onchange_order(self):
        print "Product ID",self.product_id
        self.price_unit = self.product_id.sale_price
        self.product_tax = self.product_id.tax
#         self.available_stock = self.product_id.product_stock
     
    #@api.onchange('price_unit','discount')  
    #@api.multi        
    #def _get_discount(self):   
    #    for line in self:  
    #        
    #        calc = (line.subtotal * line.discount) / 100
    #        line.subtotal = line.subtotal - calc
            
            
            
 
    @api.onchange('price_unit','quantity','discount')   
    @api.multi
    def _get_subtotal(self):   
        for line in self:
            
            line.subtotal = line.price_unit * line.quantity
            calc = (line.subtotal * line.discount) / 100
            line.subtotal = line.subtotal - calc

    
    order_id = fields.Many2one('sale.orders', string='order reference for sale')
    product_id = fields.Many2one('product.products', string = 'Product')     
    quantity = fields.Float(string = 'Quantity')
    price_unit = fields.Float('Unit Price')   
    product_tax = fields.Float('Tax')   
    subtotal = fields.Float(compute='_get_subtotal', string='Subtotal',readonly = 'True')
    discount = fields.Float(string = "Discount in %")
    create_date = fields.Datetime(string='Creation Date', readonly=True, index=True, help="Date on which sales order is created.")   

    
    

    
        

    



       

