from openerp import fields, api, models
from openerp.exceptions import UserError


class product_products(models.Model):
    
    _name= "product.products"
    
    name = fields.Char(string ="Product Name", size=100, required=True, help="Enter product name here")
    prod_type = fields.Selection([("consumable","Consumable"),("service","service")], string='Type')   
    sale_price = fields.Float("Sale Price", required=True)
    tax = fields.Float("Tax", required=True)
#     product_stock = fields.Float(string = "Product STock")

    